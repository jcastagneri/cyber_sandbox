# Trivial crack of a caesar cypher

from pycipher import Caesar

text_enc = 'qeb mxpptloa fp nnwwnnw'

for i in range(1, 26):
    print(Caesar(key=i).decipher(text_enc), i)
